package home_task_002.task_001_celsius_to_fahrenheit_converter;

public class CelsiusToFahrenheitConverter {

    public static void main(String[] args) {
        double celsius = 30.0;
        double fahrenheit = (celsius * 1.8) + 32;
        System.out.println(celsius + " °C -> " + fahrenheit + " °F");
    }
}
