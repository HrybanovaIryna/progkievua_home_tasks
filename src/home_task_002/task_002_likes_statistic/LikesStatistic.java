package home_task_002.task_002_likes_statistic;

public class LikesStatistic {
    public static void main(String[] args) {
        long likesCount = 102L;
        int registrationYear = 2014;
        int currentYear = java.time.Year.now().getValue();
        double likesPerYear = (double) (likesCount) / (currentYear - registrationYear + 1);
        System.out.println(likesPerYear + " likes / year");

    }
}
