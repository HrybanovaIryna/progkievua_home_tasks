package home_task_002.task_003_digits_printer;

public class DigitsPrinter {
    public static void main(String[] args) {
        int n = 987654321;
        int[] numbers = new int[5];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = n % 10;
            n = n / 10;
        }

        for (int i = numbers.length - 1; i >= 0; i--) {
            System.out.println(numbers[i]);
        }
    }
}
